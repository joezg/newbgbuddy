FlowRouter.triggers.enter([()=>AppMenu.clearCustomItems()]); //this will clear custom items in nav menu in layout

FlowRouter.route('/allWidgets', {
    action() {
        ReactLayout.render(App, {content() { return <AllWidgets /> }});
    }
});
FlowRouter.route('/', {
    action() {
        ReactLayout.render(App, {
            content() { return <UserWidgets /> }
        });
    }
});

FlowRouter.route('/login', {
    action(){
        ReactLayout.render(App, {content() { return <Accounts.ui.LoginFormSet redirect={()=>FlowRouter.go('/')} /> }});
    }
});

SetupPartRoutes(FlowRouter, App);

