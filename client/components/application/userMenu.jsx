var {
    MenuItem,
    } = MUI;

UserMenu = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],
    // Loads items from the Parts collection and puts them on this.data.parts
    getMeteorData() {
        return {
            user: Meteor.user()
        }
    },
    onSignOut(){
        Meteor.logout();
        AppMenu.closeMenu();
    },
    onSignIn(){
        FlowRouter.go('/login');
        AppMenu.closeMenu();
    },
    render() {
        return (
            <div>
                {this.data.user
                    ? <MenuItem onTouchTap={this.onSignOut}>Sign out ({this.data.user.username})</MenuItem>
                    : <MenuItem onTouchTap={this.onSignIn}>Sign in</MenuItem> }
            </div>
        )
    }
});