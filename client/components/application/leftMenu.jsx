const {
    LeftNav,
    MenuItem,
    Divider
    } = MUI;

AppMenu = {
    menuOpen: ReactiveVar(false),
    openMenu(){
        this.menuOpen.set(true);
    },
    closeMenu(){
        this.menuOpen.set(false);
    },
    toggleMenu(){
        this.menuOpen.set(!this.menuOpen.get());
    },
    isOpen(){
        return this.menuOpen.get();
    },
    customItems: ReactiveVar([]),
    setCustomItems(items){
        this.customItems.set(items);
    },
    clearCustomItems(){
        this.customItems.set([]);
    },
    getCustomItems(){
        return this.customItems.get();
    }
}

LeftMenu = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],
    // Loads items from the Parts collection and puts them on this.data.parts
    getMeteorData() {
        return {
            customMenuItems: AppMenu.getCustomItems(),
            isOpen: AppMenu.isOpen()
        }
    },
    onNavRequestChange(navShouldOpen){
        if (navShouldOpen)
            AppMenu.openMenu();
        else
            AppMenu.closeMenu();
    },
    renderMenuItems(){
        return this.data.customMenuItems.map((item=>{
            let wrapperCallback = function(){
                item.callback();
                AppMenu.closeMenu();
            }.bind(this);

            return <MenuItem primaryText={item.text} onTouchTap={wrapperCallback} />
        }))
    },
    render(){
        return (
            <LeftNav
                open={this.data.isOpen}
                docked={false}
                onRequestChange={this.onNavRequestChange}
            >
                <UserMenu />
                <Divider />
                {this.renderMenuItems()}
            </LeftNav>
        )
    }
})