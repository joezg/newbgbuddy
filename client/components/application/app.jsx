injectTapEventPlugin();

var { AppCanvas, AppBar } = MUI;
var { ThemeManager, LightRawTheme } = MUI.Styles;

App = React.createClass({
    childContextTypes: {
        muiTheme: React.PropTypes.object
    },
    getChildContext() {
        return {
            muiTheme: ThemeManager.getMuiTheme(LightRawTheme)
        };
    },
    render() {
        return (
            <div className="app-canvas" style={{
                display: 'flex',
                flexDirection: 'column'}}>
                <AppBar
                    title="bgBuddy"
                    style={{
                        top:0,
                        left:0,
                        position: 'fixed'
                    }}
                    onLeftIconButtonTouchTap={()=>AppMenu.toggleMenu()}
                    onTitleTouchTap={()=>FlowRouter.go('/')}
                />

                <LeftMenu />

                <main style={{
                    paddingTop: 70,
                    paddingLeft: 8,
                    paddingRight: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    flexGrow: 1
                    }}>
                    {this.props.content()}
                </main>
            </div>
        );
    }
});