//code modified from https://github.com/yuanyan/react-mq

let matchMedia = typeof window !== 'undefined' ? window.matchMedia : null;


function removeMediaQuery (mediaQuery, mediaQueryList){
    var mq = mediaQueryList[mediaQuery];
    if(mq){
        mq.mql.removeListener(mq.listener);
        mediaQueryList[mediaQuery] = null;
    }
}

MediaQueryMixin = {
    mediaQueryList: {
        mobile:  {query: '(max-width: 768px)'},
        tablet:  {query: '(min-width: 769px) and (max-width: 992px)'},
        desktop: {query: '(min-width: 993px) and (max-width: 1200px)'},
        largeDesktop: {query: '(min-width: 1201px)'}
    },

    getInitialState: function() {
        return {
            media: {}
        };
    },

    componentWillMount: function(){
        this.listenMediaQueries();
    },

    componentWillUnmount: function(){
        Object.keys(this.mediaQueryList).forEach(function(key){
            let current = this.mediaQueryList[key];
            current.mediaQueryList.removeListener(current.listener);
        }.bind(this));
    },

    setMediaQueryState: function(key, doesMatch) {
        if (this.state.media[key] != doesMatch) {
            this.state.media[key] = doesMatch;
            this.setState({
                media: this.state.media
            });
        }
    },

    listenMediaQueries: function(mediaQueries, mediaQueryLists){
        Object.keys(this.mediaQueryList).forEach(function(key){
            var query = this.mediaQueryList[key].query;
            var mediaQueryList = matchMedia(query);
            this.mediaQueryList[key].mediaQueryList = mediaQueryList;

            // Create listener
            var listener = function(mql){
                this.setMediaQueryState(key, mql.matches);
            }.bind(this);

            this.mediaQueryList[key].listener = listener;

            listener(mediaQueryList);

            // Listen for changes
            mediaQueryList.addListener(listener);
        }.bind(this));
    }
};