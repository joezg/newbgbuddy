Meteor.publish('scorer:games', function(){
    check(this.userId, String);
    return Games.find({userId: this.userId});
});
