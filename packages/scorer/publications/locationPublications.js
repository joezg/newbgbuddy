Meteor.publish('scorer:locations', function(){
    check(this.userId, String);
    return Locations.find({userId: this.userId});
});
