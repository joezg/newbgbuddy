Meteor.publish('scorer:matches', function(){
    check(this.userId, String);
    return Matches.find({userId: this.userId});
});
