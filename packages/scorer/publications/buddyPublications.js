Meteor.publish('scorer:buddies', function(){
    check(this.userId, String);
    return Buddies.find({userId: this.userId});
});
