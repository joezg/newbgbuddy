Meteor.publish('scorer:groups', function(){
    check(this.userId, String);
    return Groups.find({userId: this.userId});
});
