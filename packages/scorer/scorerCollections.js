Matches = new Mongo.Collection('scorerMatches');
Games = new Mongo.Collection('scorerGames');
Locations = new Mongo.Collection('scorerLocations');
Groups = new Mongo.Collection('scorerGroups');
Buddies = new Mongo.Collection('scorerBuddies');
