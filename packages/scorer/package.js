Package.describe({
  name: 'jhladek:scorer',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use(['ecmascript', 'mongo', 'check'], ['client', 'server']);
  api.use([
    'react',
    'jhladek:parts-core',
    'izzilab:material-ui',
    'jhladek:react-media-query',
    'jhladek:utility-components',
    'jhladek:scorer-ui-core',
    'jhladek:extensible-form'
  ], 'client')
  api.addFiles([
    'scorerCollections.js',
    'methods/gameMethods.js',
    'methods/groupMethods.js',
    'methods/buddyMethods.js',
    'methods/locationMethods.js',
    'methods/matchMethods.js'
  ], ['client', 'server']);
  api.addFiles([
    'models/matchModel.js',
    'components/match/matchFormEmpty.jsx',
    'components/match/complexMatchForm.jsx',
    'components/match/matchList.jsx',
    'components/match/scoreExtension.jsx',
    'components/match/matchBasicInfo.jsx',
    'components/buddies/buddyPicker.jsx',
    'components/itemPicker.jsx',
    'scorerParts.jsx'
  ], 'client');
  api.addFiles([
    'publications/gamePublications.js',
    'publications/groupPublications.js',
    'publications/locationPublications.js',
    'publications/buddyPublications.js',
    'publications/matchPublications.js'
  ], 'server');
  api.export(['Games', 'Locations', 'Groups', 'Buddies', 'Matches' ], ['client', 'server']);
});