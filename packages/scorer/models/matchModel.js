MatchModel = function(match){
    this.match = match;
}

MatchModel.prototype.getWinners = function(){
    return _.filter(this.match.players, (p)=>{return p.winner});
}
