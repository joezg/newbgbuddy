AddPart({
    _id: "complex-match-form",
    routeGroup: "match",
    category: "Scorer",
    title: "Record match",
    subtitle: "Save details of a match you played",
    description: "With this widget you can add really great amount of details to your recorded matches",
    routes: {
        "/": ComplexMatchForm,
        "/:matchId": ComplexMatchForm
    }
});

AddPart({
    _id: "matches",
    routeGroup: "matches",
    category: "Stats",
    title: "Played matches",
    subtitle: "View all of them",
    description: "Analyze all your played matches and edit them if necessary",
    routes: {
        "/": MatchList
    }
});