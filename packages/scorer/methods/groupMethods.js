Meteor.methods({
    "scorer:addGroup"(group){
        check(Meteor.userId(), String);
        check(group, {
            name: String,
            imageLink: Match.Optional(String)
        })


        group.userId = Meteor.userId();

        let _id = Groups.insert(group);

        return {_id};
    }
})