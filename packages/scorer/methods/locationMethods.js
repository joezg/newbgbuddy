Meteor.methods({
    "scorer:addLocation"(location){
        check(Meteor.userId(), String);
        check(location, {
            name: String,
            imageLink: Match.Optional(String),
            isHome:  Match.Optional(Boolean)
        })


        location.userId = Meteor.userId();

        let _id = Locations.insert(location);

        return {_id};
    }
})