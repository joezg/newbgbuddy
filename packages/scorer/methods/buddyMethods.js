Meteor.methods({
    "scorer:addBuddy"(buddy){
        check(Meteor.userId(), String);
        check(buddy, {
            name: String,
            imageLink: Match.Optional(String),
            isUser: Match.Optional(Boolean)
        })


        buddy.userId = Meteor.userId();

        let _id = Buddies.insert(buddy);

        return {_id};
    }
})