Meteor.methods({
    "scorer:addGame"(game){
        check(Meteor.userId(), String);
        check(game, {
            name: String,
            imageLink: Match.Optional(String)
        })


        game.userId = Meteor.userId();

        let _id = Games.insert(game);

        return {_id};
    },
    "scorer:updateGameScorer"(_id, scorerId){
        check(Meteor.userId(), String);
        check(_id, String);
        check(scorerId, String);

        let game = Games.findOne({_id, userId:Meteor.userId()});

        if (game){
            Games.update({_id}, {$set: {scorerId}});
        }
    }
})