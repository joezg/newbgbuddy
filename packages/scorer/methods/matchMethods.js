Meteor.methods({
    "scorer:createMatch"(date){
        check(Meteor.userId(), String);
        check(date, Match.Optional(Date));

        date = date || new Date();

        let match = {
            userId: Meteor.userId(),
            date
        };

        let _id = Matches.insert(match);

        return {_id};
    }
});

Meteor.methods({
    "scorer:updateMatchLocation"(_id, locationId, locationName){
        updateField(_id, locationId, locationName, 'location');
    },
    "scorer:resetMatchLocation"(_id){
        resetField(_id, 'location');
    },
    "scorer:updateMatchGame"(_id, gameId, gameName){
        updateField(_id, gameId, gameName, 'game');

        let game = Games.findOne(gameId);
        let match = Matches.findOne({_id, userId:Meteor.userId() });

        if (match && !match.isScored){
            if (game.scorerId){
                Matches.update({_id}, {$set: {scorerId: game.scorerId}})
            } else {
                Matches.update({_id}, {$unset: {scorerId: true}})
            }
        }


    },
    "scorer:resetMatchGame"(_id){
        resetField(_id, 'game');
    },
    "scorer:updateMatchGroup"(_id, groupId, groupName){
        updateField(_id, groupId, groupName, 'group');
    },
    "scorer:resetMatchGroup"(_id){
        resetField(_id, 'group');
    },
    "scorer:addMatchPlayer"(_id, player){
        check(Meteor.userId(), String);
        check(_id, String);
        check(player, Match.ObjectIncluding({
            name: String,
            _id: String
        }));

        let match = Matches.findOne({_id, userId:Meteor.userId()});

        if (match){
            Matches.update({_id}, {$push: {players: {buddyId: player._id, name: player.name}}});
        }

    },
    "scorer:removeMatchPlayer"(_id, player){
        check(Meteor.userId(), String);
        check(_id, String);
        check(player, Match.ObjectIncluding({
            name: String,
            _id: String
        }));

        let match = Matches.findOne({_id, userId:Meteor.userId()});

        if (match){
            Matches.update({_id}, {$pull: {players: {buddyId: player._id}}});
        }
    },
    "scorer:updateMatchDate"(_id, date){
        check(Meteor.userId(), String);
        check(_id, String);
        check(date, Date);

        let match = Matches.findOne({_id, userId:Meteor.userId()});

        if (match){
            Matches.update({_id}, {$set: {date}});
        }
    },
    "scorer:updateMatchPlayers"(_id, players, isScored){
        check(Meteor.userId(), String);
        check(_id, String);
        check(isScored, Boolean)
        check(players, [
            Match.ObjectIncluding({
                name: String,
                buddyId: String,
                score: Number,
                rank: Match.Optional(Number)
            })
            ]
        );

        let match = Matches.findOne({_id, userId:Meteor.userId()});

        if (match){
            Matches.update({_id}, {$set: {players, isScored}});
        }

    },
    "scorer:updateMatchScorer"(_id, scorerId){
        check(Meteor.userId(), String);
        check(_id, String);
        check(scorerId, String);

        let match = Matches.findOne({_id, userId:Meteor.userId()});

        if (match){
            Matches.update({_id}, {$set: {scorerId}});
        }
    }
});

let updateField = function(_id, itemId, itemName, itemField){
    check(Meteor.userId(), String);
    check(_id, String);
    check(itemId, String);
    check(itemName, String);

    let match = Matches.findOne({_id, userId:Meteor.userId()});

    if (match){
        let $set = {}
        $set[itemField+'Id'] = itemId;
        $set[itemField+'Name'] = itemName;
        Matches.update({_id}, {$set: $set});
    }
};

let resetField = function(_id, itemField){
    check(Meteor.userId(), String);
    check(_id, String);

    let match = Matches.findOne({_id, userId:Meteor.userId()});

    if (match){
        let $unset = {};
        $unset[itemField+'Id'] = true;
        $unset[itemField+'Name'] = true;
        Matches.update({_id}, {$unset: $unset});
    }
}