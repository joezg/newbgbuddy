let { FlatButton, Paper } = MUI;

MatchFormEmpty = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    // Loads items from the Parts collection and puts them on this.data.parts
    getMeteorData() {
        let data = {
            locations: [],
            games: [],
            groups: [],
            buddies: [],
            match: null,
            loadingDone: false
        }

        let gameHandle = Meteor.subscribe('scorer:games');
        if (gameHandle.ready()) {
            data.games = Games.find().fetch()
        }
        let buddyHandle = Meteor.subscribe('scorer:buddies');
        if (buddyHandle.ready()) {
            data.buddies = Buddies.find().fetch()
        }
        let locationHandle = Meteor.subscribe('scorer:locations');
        if (locationHandle.ready()) {
            data.locations = Locations.find().fetch()
        }
        let groupHandle = Meteor.subscribe('scorer:groups');
        if (groupHandle.ready()) {
            data.groups = Groups.find().fetch()
        }
        let matchHandle = Meteor.subscribe('scorer:matches');
        if (matchHandle.ready()) {
            data.match = Matches.findOne(this.props.matchId);
        }

        if (gameHandle.ready() && buddyHandle.ready() && locationHandle.ready() && matchHandle.ready()){
            data.loadingDone = true;
        }

        return data
    },
    getDefaultProps(){
        return {
            title: 'Record a match',
            subtitle: 'by entering as much or as little data as you want'
        }
    },
    _getMatch(cb){
        if (this.data.match){
            cb(this.data.match);
        } else {
            Meteor.call("scorer:createMatch", (err, res)=>{
                cb(Matches.findOne(res._id));
                FlowRouter.go('/match/'+res._id);
            });
        }
    },
    render(){
        return (
            <IsLogged>
                <LoadingWait waitFor={this.data.loadingDone}>
                    <ExtensibleForm {...this.props} {...this.data} onMatchNeeded={this._getMatch} />
                </LoadingWait>
            </IsLogged>

        );
    }
})