ScoreExtension = React.createClass({
    handleScoreChanged(isScored, newScores){
        Meteor.call('scorer:updateMatchPlayers', this.props.match._id, newScores, isScored);
    },
    handleSettingsChanged(changedCategory){
        console.log('settings changed');
    },
    handleScorerChanged(newId){
        Meteor.call('scorer:updateMatchScorer', this.props.match._id, newId);
        if (this.props.match.gameId){
            Meteor.call('scorer:updateGameScorer', this.props.match.gameId, newId);
        }
    },

    render(){
        return (
            <ScoreTable
                scorerId={this.props.match.scorerId}
                scores={this.props.match.players}
                isScored={this.props.match.isScored}
                firstColumnWidth={150}
                columnWidth={100}
                onScoreChanged={this.handleScoreChanged.bind(this, true)}
                onScoreReset={this.handleScoreChanged.bind(this, false)}
                onSettingsChanged={this.handleSettingsChanged}
                onScorerChanged={this.handleScorerChanged}
            />
        )
    }
})