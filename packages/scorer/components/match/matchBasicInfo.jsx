let { DatePicker } = MUI;

MatchBasicInfo = React.createClass({
    _getMatch(cb){
        if (this.props.match){
            cb(this.props.match)
        } else {
            this.props.onMatchNeeded(cb);
        }
    },
    handleItemSelected(field, item){
        this._getMatch((match)=>{
            Meteor.call("scorer:updateMatch"+field, match._id, item._id, item.name);
        });
    },
    handleItemReset(field){
        this._getMatch((match)=>{
            Meteor.call("scorer:resetMatch"+field, match._id);
        });
    },
    handleNewItem(field, newName){
        this._getMatch((match)=>{
            let newItem = {name: newName};
            Meteor.call("scorer:add"+field, newItem, function(err, res){
                if (!err){
                    newItem._id = res._id;
                    this.handleItemSelected(field, newItem);
                }
            }.bind(this));
        });
    },
    handleDateChange(unimportant, date){
        this._getMatch((match)=>{
            Meteor.call("scorer:updateMatchDate", match._id, date);
        });
    },
    render(){
        let locationId = null;
        let gameId = null;
        let groupId = null;
        let buddyIds = [];
        let playerPickingEnabled = true;

        if (this.props.match){
            locationId = this.props.match.locationId;
            gameId = this.props.match.gameId;
            groupId = this.props.match.groupId;
            buddyIds = this.props.match.players ? this.props.match.players.map((i)=>i.buddyId) : [];
            playerPickingEnabled = this.props.match.playerPickingEnabled === false ? false : true;
        }

        return (
            <div>
                <span>
                    <h3 style={{float: "left", marginRight: 10}}>Date:</h3>
                    <DatePicker
                        autoOk={true}
                        mode="portrait"
                        value={this.props.match?this.props.match.date:null}
                        onChange={this.handleDateChange}
                    />
                </span>
                <ItemPicker
                    title="Location"
                    items={this.props.locations}
                    defaultIcon="near_me"
                    onItemSelected={this.handleItemSelected.bind(this, 'Location')}
                    selectedItemId={locationId}
                    onItemReset={this.handleItemReset.bind(this, 'Location')}
                    onNewItem={this.handleNewItem.bind(this, 'Location')}
                />
                <ItemPicker
                    title="Game"
                    items={this.props.games}
                    defaultIcon="event_seat"
                    onItemSelected={this.handleItemSelected.bind(this, 'Game')}
                    selectedItemId={gameId}
                    onItemReset={this.handleItemReset.bind(this, 'Game')}
                    onNewItem={this.handleNewItem.bind(this, 'Game')}
                />
                <ItemPicker
                    title="Group"
                    items={this.props.groups}
                    defaultIcon="group"
                    onItemSelected={this.handleItemSelected.bind(this, 'Group')}
                    selectedItemId={groupId}
                    onItemReset={this.handleItemReset.bind(this, 'Group')}
                    onNewItem={this.handleNewItem.bind(this, 'Group')}
                />
            </div>
        )
    }
})