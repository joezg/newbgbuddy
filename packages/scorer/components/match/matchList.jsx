let { FlatButton, Paper, Avatar, FontIcon, RaisedButton, Popover, List, ListItem } = MUI;

MatchList = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    // Loads items from the Parts collection and puts them on this.data.parts
    getMeteorData() {
        let data = {
            matches: [],
            loadingDone: false
        }

        let matchHandle = Meteor.subscribe('scorer:matches');
        if (matchHandle.ready()) {
            data.matches = Matches.find().fetch();
            data.loadingDone = true;
        }

        return data
    },
    getDefaultProps(){
        return {
            title: 'Played matches',
            subtitle: 'place to view all matches and edit them if necessary'
        }
    },
    getInitialState(){
        return {
            playersPopoverAnchor: null,
            playersToShow: [],
            showPlayers: false
        }
    },
    getExtensions(){
        return {
            back: {
                actionName: "Back",
                actionEnabled: true,
                action: () => { FlowRouter.go('/') }
            },
            matches: {
                actionName: "Matches",
                actionEnabled: true,
                rendering: this.renderMatches()
            },
        }
    },
    getMatchWinners(match){
        let winners = (new MatchModel(match)).getWinners();
        return winners.map((w)=>{
            return <Paper style={{padding: 5}}>{w.name}</Paper>
        })
    },
    handleEditMatchClicked(match, event){
        event.preventDefault();
        FlowRouter.go('/match/'+match._id);
    },
    handleShowPlayersClick(players, event){
        event.preventDefault();

        this.state.playersAnchor = event.currentTarget;
        this.state.playersToShow = players;
        this.state.showPlayers = true;
        this.setState(this.state);
    },
    renderMatch(match){
        let avatarIconStyle = {fontSize: 15, marginTop: 5, marginBottom: 0, marginLeft: 0, marginRight: 0},
            detailsStyle = {margin:5},
            avatarDefaultProps = {
                size: 25,
                style: {
                    marginRight: 10
                }
            }

        return (
            <Paper style={{height:205, width: 200, margin:10, padding:10, display:'flex', flexDirection:'column'}}>
                <div>{match.gameName ? match.gameName : 'unknown game'}</div>
                <div style={{flexGrow:1, marginTop:10}}>
                    <div style={detailsStyle}>
                        <Avatar {...avatarDefaultProps} icon={<FontIcon style={avatarIconStyle} className="material-icons">near_me</FontIcon>}/>
                        {match.locationName ? match.locationName : 'unknown'}
                    </div>
                    <div style={detailsStyle}>
                        <Avatar {...avatarDefaultProps} icon={<FontIcon style={avatarIconStyle} className="material-icons">group</FontIcon>}/>
                        {match.groupName ? match.groupName : 'unknown'}</div>

                    {match.players ? <RaisedButton onTouchTap={this.handleShowPlayersClick.bind(this, match.players)} style={detailsStyle} label={match.players.length + ' players'}></RaisedButton> : null }

                </div>
                <div><FlatButton onTouchTap={this.handleEditMatchClicked.bind(this, match)}>Edit</FlatButton></div>
            </Paper>
        )
    },
    renderPlayersPopover(){
        return (
            <Popover
                open={this.state.showPlayers}
                anchorEl={this.state.playersAnchor}
                anchorOrigin={{horizontal: 'middle', vertical: 'bottom'}}
                targetOrigin={{horizontal: 'middle', vertical: 'top'}}
                canAutoPosition={false}
                onRequestClose={()=>{
                        this.state.showPlayers = false;
                        this.state.playersAnchor = null;
                        this.setState(this.state);
                    }
                }
            >
                <List>
                    {this.state.playersToShow.map((p)=>{
                        let avatar;

                        if (p.imageLink){
                            avatar = <Avatar src={p.imageLink} />
                        } else {
                            avatar = <Avatar><FontIcon style={{top:3}} className="material-icons">face</FontIcon></Avatar>
                        }

                        return (<ListItem primaryText={p.name} leftAvatar={avatar}/>);
                    })}
                </List>
            </Popover>
        )
    },
    renderMatches(){
        return (
            <div>
                <div style={{display: 'flex', flexDirection: 'row', flexWrap:'wrap', justifyContent: 'center'}}>
                    {this.data.matches.map((m)=>{
                        return this.renderMatch(m);
                    })}
                </div>
                {this.renderPlayersPopover()}
            </div>

        )
    },
    render(){
        return (
            <IsLogged>
                <LoadingWait waitFor={this.data.loadingDone}>
                    <ExtensibleForm {...this.props} {...this.data} extensions={this.getExtensions()} extensionToShowId="matches" />
                </LoadingWait>
            </IsLogged>

        );
    }
})