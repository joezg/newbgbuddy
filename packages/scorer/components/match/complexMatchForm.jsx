ComplexMatchForm = React.createClass({
    render(){
        let extensions = {
            finish: {
                actionName: "Finish",
                actionEnabled: true,
                action: () => { FlowRouter.go('/') }
            },
            basicInfo: {
                actionName: "Basics",
                actionEnabled: true,
                component: MatchBasicInfo
            },
            playerPicker :{
                actionName: "Players",
                actionEnabled: true,
                component: BuddyPicker
            },
            scoreTable :{
                actionName: "Scores",
                actionEnabled: function(props){
                    return props.match && props.match.players && props.match.players.length > 0;
                },
                component: ScoreExtension
            },
            memosForm :{
                actionName: "Memos",
                actionEnabled: false
            },
            commentForm :{
                actionName: "Comments",
                actionEnabled: false
            },
            matchPeriods :{
                actionName: "Times",
                actionEnabled: false
            }
        }

        return (
            <MatchFormEmpty {...this.props} extensions={extensions} extensionToShowId="basicInfo" />
        )
    }
});