let {
    GridList,
    GridTile,
    FontIcon,
    FlatButton,
    Paper,
    TextField
    } = MUI;

let centered = {
    textAlign: 'center'
}

let fontIconStyle = {
    fontSize: 46

}

ItemPicker = React.createClass({
    mixins: [MediaQueryMixin],
    getInitialState(){
        return {
            selectedItems: [],
            searchQuery: "",
            cannotAdd: false
        }
    },
    getDefaultProps(){
        return {
            selectedItemIds: [],
            selectedItemId: null,
            items: [],
            multiselect: false,
            onItemSelected: ()=>{},
            onItemReset: ()=>{},
            defaultIcon: 'face',
            donePicking: false,
            onPickingDone: ()=>{},
            onPickingRestarted: ()=>{},
            onNewItem: ()=>{}
        }
    },
    getSelectedItem(){
        return this.props.items.find((i)=> i._id === this.props.selectedItemId);
    },
    handleItemClicked(item, e){
        if (!this.props.multiselect || !this.props.donePicking)
            this.props.onItemSelected(item);
    },
    handleChangeClicked(){
        this.props.onItemReset();
        this.state.searchQuery = "";
        this.setState(this.state);
    },
    handleDoneClicked(){
        this.props.onPickingDone();
    },
    handleEditClicked(){
        this.props.onPickingRestarted();
    },
    handleSearchAddChange(){
        this.state.searchQuery = this.refs.searchAddField.getValue();
        this.state.cannotAdd = false;
        this.setState(this.state);
    },
    handleAddClicked(){
        let name = this.refs.searchAddField.getValue();
        this.props.onNewItem(name);
        this.handleClearClicked();
    },
    handleClearClicked(){
        this.refs.searchAddField.setValue("");
        this.state.searchQuery = "";
        this.setState(this.state);
    },
    renderItems(){
        return this.props.items.reduce(function(memo, item) {
            let content = null;

            if (item.imageLink){
                content = <img src={item.imageLink} />
            } else {
                content = <div style={centered} ><FontIcon className="material-icons" style={fontIconStyle}>{item.icon ? item.icon : this.props.defaultIcon}</FontIcon></div>
            }

            let depth = 1;
            let opacity = 1;
            let itemSelected = false;
            if (this.props.selectedItemIds.indexOf(item._id)>-1){
                if (!this.props.donePicking) depth = 5;
                itemSelected = true;
            } else {
                if (this.props.multiselect){
                    opacity = 0.5;
                }
            }

            //searching
            let itemFilteredOut = false;
            if (this.state.searchQuery && !itemSelected && item.name.toLowerCase().indexOf(this.state.searchQuery.toLowerCase()) === -1){
                itemFilteredOut = true;
            }

            if (this.state.searchQuery && !this.state.cannotAdd && item.name.toLowerCase() === this.state.searchQuery.toLowerCase()){
                this.state.cannotAdd = true;
                this.setState(this.state);
            }

            if (!itemFilteredOut && (!this.props.multiselect || !this.props.donePicking || itemSelected))
                memo.push(
                    <GridTile
                        key={item._id}
                        title={item.name}
                        rootClass={Paper}
                        zDepth={depth}
                        style={{opacity}}
                        onTouchTap={this.handleItemClicked.bind(this, item)}
                    >
                        {content}
                    </GridTile>
                )

            return memo;
        }.bind(this), [])

    },
    render(){
        let cols;
        if (this.state.media.mobile){
            cols = 3;
        } else if (this.state.media.tablet){
            cols = 6;
        } else if (this.state.media.desktop){
            cols = 8;
        } else {
            cols = 10;
        }

        let itemName = null;
        let titleOptions = null;
        let itemsVisible = true;
        if (this.props.multiselect){
            titleOptions =
                <span>
                    {this.props.donePicking ? null : <FlatButton onTouchTap={this.handleDoneClicked} primary={true}>Done</FlatButton>}
                    {this.props.donePicking ? <FlatButton onTouchTap={this.handleEditClicked} primary={true}>Edit</FlatButton> : null}
                </span>;
        } else if(this.props.selectedItemId ) {
            itemName = <span style={{fontWeight: "normal"}}>: {this.getSelectedItem().name}</span>;
            titleOptions = <FlatButton onTouchTap={this.handleChangeClicked} primary={true}>Change</FlatButton>
            itemsVisible = false;
        }

        let itemsComponent = null;
        if (itemsVisible){
            if (this.props.items.length === 0){
                itemsComponent = <div><em>Not one item exists. Add it below!</em></div>
            } else {
                itemsComponent =
                    <GridList
                        cellHeight={100}
                        cols={cols}
                        padding={8}
                        style={{minHeight:100}}
                    >
                        {this.renderItems()}
                    </GridList>
            }
        }

        let searchAddComponent =
            itemsVisible && !this.props.donePicking ?
                <div>
                    <TextField ref="searchAddField" onChange={this.handleSearchAddChange}
                        hintText="Add new / Search"
                    />
                    {this.state.searchQuery ? <FlatButton onTouchTap={this.handleClearClicked} primary={true}>Clear</FlatButton> : null}
                    {this.state.searchQuery && !this.state.cannotAdd ? <FlatButton secondary={true} onTouchTap={this.handleAddClicked}>Add</FlatButton> : null}
                </div>
                : null;

        return (
            <div>
                <h3>{this.props.title}{itemName} {titleOptions}</h3>
                { itemsComponent }
                { searchAddComponent }
            </div>
        );
    }
})