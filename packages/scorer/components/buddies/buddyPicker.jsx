let {List, ListItem, Avatar, FontIcon, IconButton} = MUI;

BuddyPicker = React.createClass({
    _getMatch(cb){
        if (this.props.match){
            cb(this.props.match)
        } else {
            this.props.onMatchNeeded(cb);
        }
    },
    handleBuddySelected(item){
        this._getMatch((match)=>{
            Meteor.call("scorer:addMatchPlayer", match._id, item);
        })
    },
    handleNewItem(newName){
        let newItem = {name: newName};
        Meteor.call("scorer:addBuddy", newItem, function(err, res) {
            if (!err) {
                newItem._id = res._id;
                this.handleBuddySelected(newItem);
            }
        }.bind(this))
    },
    handleBuddyRemoved(item){
        this._getMatch((match)=>{
            Meteor.call("scorer:removeMatchPlayer", match._id, {name:item.name, _id: item.buddyId});
        })
    },
    _getBuddiesInMatch(){
        let players = this.props.match ? this.props.match.players : null;

        if (!players)
            players = []

        return players;
    },
    getFilteredBuddies(){
        return _.filter(this.props.buddies, (buddy)=>{
            return !_.some(this._getBuddiesInMatch(), (player)=>{
                return player.buddyId === buddy._id;
            })
        });
    },
    renderBuddies(){
        return this._getBuddiesInMatch().map((p)=>{
            let avatar;

            if (p.imageLink){
                avatar = <Avatar src={p.imageLink} />
            } else {
                avatar = <Avatar><FontIcon style={{top:3}} className="material-icons">face</FontIcon></Avatar>
            }

            let removeButton =
                <IconButton onTouchTap={this.handleBuddyRemoved.bind(this, p)}>
                    <FontIcon className="material-icons">clear</FontIcon>
                </IconButton>;

            return (<ListItem primaryText={p.name} leftAvatar={avatar} rightIconButton={removeButton} />);
        })
    },
    render(){
        return (
            <div>
                <ItemPicker
                    title="Select buddies for this match"
                    items={this.getFilteredBuddies()}
                    defaultIcon="face"
                    onItemSelected={this.handleBuddySelected}
                    onNewItem={this.handleNewItem}
                />
                <h3>Selected buddies</h3>
                <List>
                    {this.renderBuddies()}
                </List>

            </div>

        );
    }
})
