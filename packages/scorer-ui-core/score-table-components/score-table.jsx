const {Popover, Paper, SelectField, MenuItem, Divider, FlatButton} = MUI;

ScoreTable = React.createClass({
    getInitialState(){
        return {
            currentScore: null,
            currentCategory: null,
            popoverActive: false,
            popoverType: null,
            scores: [],
            categories: [],
            settings: {}
        };
    },
    getDefaultProps(){
        return {
            scores: [],
            transposed: false,
            firstColumnWidth: 200,
            columnWidth: 150,
            headerHeight: 59,
            onScoreChanged: ()=>{},
            onScoreReset: ()=>{},
            onSettingsChanged: ()=>{},
            onScorerChanged: ()=>{}
        }
    },
    updateStateWithProps(props, updateScorer){
        if (updateScorer){
            if (!props.scorerId){
                this.state.popoverType = 'settings';
                this.state.popoverActive = true;
                this.state.scorerId = 'defaultScorer'
                this.props.onScorerChanged(this.state.scorerId);
            } else {
                this.state.scorerId = props.scorerId;
            }

            let Scorer = Scorers[this.state.scorerId].scorer;
            let currentScorer = new Scorer();

            this.state.scorer = currentScorer;
            this.state.categories = currentScorer.categories;
            this.state.settings = currentScorer.settings;
        }

        this.state.scores = props.scores;
        this.setState(this.state);
    },
    componentWillReceiveProps(nextProps){
        this.updateStateWithProps(nextProps, false);
    },
    componentWillMount(){
        this.updateStateWithProps(this.props, true);
    },
    _closePopover(){
        this.state.currentScore = null;
        this.state.currentCategory = null;
        this.state.popoverActive = false;
        this.state.popoverType = null;
        this.settingsChangedWhileScoringPopoverOpen = false;
        this.setState(this.state);
    },
    handleScoreEditRequested(score, category){
        this.state.currentScore = score;
        this.state.currentCategory = category;
        this.state.popoverType = 'score';
        this.state.popoverActive = true;
        this.settingsChangedWhileScoringPopoverOpen = false;
        this.setState(this.state);
    },
    handleSettingsClicked(){
        this.state.popoverType = 'settings';
        this.state.popoverActive = true;
        this.setState(this.state);
    },
    handleScoreChanged(score, category, newScore){
        this.state.scorer.scoreChanged(this.state.scores, score, category, newScore);
        this.setState(this.state);
    },
    handleScoreChangeCompleted(){
        this.state.scorer.scoreChangeCompleted(this.state.scores);
        this.setState(this.state);
        this.props.onScoreChanged(this.state.scores);
        this._closePopover();
    },
    handleSettingsChanged(newSettings){
        this.state.scorer.settingsChanged(newSettings);
        this.state.settings = this.state.scorer.settings;
        this.setState(this.state);

        if (this.state.popoverActive && this.state.popoverType === 'score'){
            this.settingsChangedWhileScoringPopoverOpen = true;
        }
    },
    handleSettingsChangeCompleted(calculateScoresAndClosePopover){
        this.props.onSettingsChanged(this.state.settings);
        //only in case of global scorer settings are changed scores will be racalculated.
        //category scores change never recalculates scores. it is used mainly for UI widgets.
        //if you need to update scores when setting change don't put settings in scoreEdit widget
        //but in scorer setting dialog
        //Also, rescoring is not triggered when match isn't scored. Any change in scores will update
        //this flag appropriately
        if (calculateScoresAndClosePopover !== false){
            this.props.isScored && this.handleScoreChangeCompleted();
            this._closePopover();
        }

    },
    handlePopoverDone(){
        if (this.state.popoverType === 'score'){
            if (this.settingsChangedWhileScoringPopoverOpen){
                let calculateScoresAndClosePopover = false;
                this.handleSettingsChangeCompleted(calculateScoresAndClosePopover);
            }
            this.handleScoreChangeCompleted();
        } else if (this.state.popoverType === 'settings'){
            let calculateScoresAndClosePopover = true;
            this.handleSettingsChangeCompleted(calculateScoresAndClosePopover);
        }
    },
    _resetScores(){
        this.state.scores = this.state.scores.map((s)=>{
            let newPlayer = _.pick(s, 'name', 'buddyId');
            newPlayer.score = 0;
            newPlayer.winner = false;
            newPlayer.rank = 0;
            return newPlayer;
        });
    },
    handleScorerChange(event, index, newId){
        event.preventDefault();

        let Scorer = Scorers[newId].scorer;
        let currentScorer = new Scorer();

        this.state.scorerId = newId;
        this.state.scorer = currentScorer;
        this.state.categories = currentScorer.categories;
        this.state.settings = currentScorer.settings;

        this._resetScores();
        this.setState(this.state);

        this.props.onScoreReset(this.state.scores);
        this.props.onScorerChanged(newId);
    },
    handleScoresReset(){
        this._resetScores();
        this.setState(this.state);

        this.props.onScoreReset(this.state.scores);
    },
    getScorers(){
        return _.values(Scorers);
    },
    renderPopover(){

        var popoverComponent = null;
        if (this.state.popoverActive && this.state.popoverType === 'score'){
            const ScoreComponent = this.state.currentCategory.editComponent;
            popoverComponent =
                <Paper style={{padding:10}}>
                    <div style={{marginBottom:10}}>
                        {this.state.currentScore.name} / {this.state.currentCategory.label}
                    </div>
                    <ScoreComponent
                        score={this.state.currentScore}
                        category={this.state.currentCategory}
                        settings={this.state.settings}
                        onScoreChangeCompleted={this.handleScoreChangeCompleted}
                        onScoreChanged={this.handleScoreChanged}
                        onSettingsChanged={this.handleSettingsChanged}
                        onSettingsChangeCompleted={this.handleSettingsChangeCompleted.bind(this, false)}
                    />
                </Paper>
        } else if (this.state.popoverActive && this.state.popoverType === 'settings'){
            const SettingsComponent = this.state.scorer ? this.state.scorer.settingsComponent : null;
            popoverComponent =
                <Paper style={{padding:15, overflowY:'hidden'}}>
                    <div style={{fontWeight: 'bold'}}>Setup scorer</div>
                    <SelectField value={this.state.scorerId || 'defaultScorer'} floatingLabelText="Pick scorer type" onChange={this.handleScorerChange}>
                        {this.getScorers().map((scorer)=><MenuItem value={scorer._id} primaryText={scorer.name}/>)}
                    </SelectField>
                    {SettingsComponent ?
                        <div>
                            <div style={{fontWeight: 'bold', marginTop:20, marginBottom:20}}>Specific scorer options</div>
                            <SettingsComponent
                                onSettingsChangeCompleted={this.handleSettingsChangeCompleted.bind(this, true)}
                                onSettingsChanged={this.handleSettingsChanged}
                                settings={this.state.scorer.settings}
                            />
                        </div> : null}
                </Paper>
        }

        return popoverComponent;
    },
    render(){
        return (
            <div ref={(component)=> this.scoreTableWrapper = component} id="score-table-wrapper" style={{textAlign: 'center', flexGrow:1, display: 'flex', flexDirection: 'column', overflow:'hidden'}}>
                {!this.state.popoverActive ?
                    <InnerDataTable
                        {...(_.pick(this.props, 'headerHeight', 'firstColumnWidth', 'columnWidth'))}
                        scores={this.state.scores}
                        categories={this.state.categories}
                        settings={this.state.settings}
                        isScored={this.props.isScored}
                        onScoreEditRequested={this.handleScoreEditRequested}
                        onScoreChanged={this.handleScoreChanged}
                        onScoreChangeCompleted={this.handleScoreChangeCompleted}
                        onSettingsClicked={this.handleSettingsClicked}
                        onScoresReset={this.handleScoresReset}
                    /> : null}
                <Popover
                    style={{maxWidth:'80%'}}
                    anchorEl={ReactDOM.findDOMNode(this.scoreTableWrapper)}
                    open={this.state.popoverActive}
                    anchorOrigin={{"horizontal":"middle","vertical":"center"}}
                    targetOrigin={{"horizontal":"middle","vertical":"center"}}
                    onRequestClose={this.handlePopoverDone}
                >
                    {this.renderPopover()}
                </Popover>
            </div>
        );
    }
});