const {Table, TableHeader, TableRow, TableHeaderColumn, TableBody, TableRowColumn, Popover, Paper, FontIcon, IconButton} = MUI;

InnerDataTable = React.createClass({
    componentDidMount(){
        //scrolling of all parts for frozen columns and rows
        const dataTable = ReactDOM.findDOMNode(this.refs.dataTable.refs.tableDiv),
            dataTableWrapper = ReactDOM.findDOMNode(this.refs.dataTable),
            rowHeader = ReactDOM.findDOMNode(this.refs.rowHeader.refs.tableDiv),
            firstColumn = ReactDOM.findDOMNode(this.refs.firstColumn.refs.tableDiv);

        const rowWidth = rowHeader.clientWidth,
            columnHeight = firstColumn.clientHeight;

        const adjustScroll = function(){
            //inspiration found here: http://tinf2.vub.ac.be/~dvermeir/manual/freezing-table-headers.html
            var h_delta = dataTable.scrollLeft;
            rowHeader.style.width = (rowWidth+h_delta) + "px";
            rowHeader.style.left = (0-h_delta) + "px";
            var v_delta = dataTableWrapper.scrollTop;
            firstColumn.style.height = (columnHeight+v_delta) + "px";
            firstColumn.style.top = (0-v_delta) + "px";
        };

        dataTable.addEventListener("scroll", adjustScroll);
        dataTableWrapper.addEventListener("scroll", adjustScroll);
    },
    handleScoreEditRequested(score, category){
        this.props.onScoreEditRequested(score, category);
    },
    handleScoreChanged(score, category, newValue){
        this.props.onScoreChanged(score, category, newValue);
    },
    handleSettingsClicked(event){
        event.preventDefault();
        this.props.onSettingsClicked();
    },
    handleClearClicked(event){
        event.preventDefault();
        this.props.onScoresReset()
    },
    getHeaders(){
        let headerStyle={
            textAlign: 'center'
        }

        let collection = this.props.categories;
        let field = 'label';

        if (this.props.settings.transpose){
            collection = this.props.scores;
            field = 'name';
        }

        return collection.map((item)=>{
            let textStyle = {};
            if (!this.props.settings.transpose && item.isSummary){
                textStyle.color = MUI.Styles.getMuiTheme().baseTheme.palette.accent1Color;;
            } else if (!this.props.settings.transpose && item.isEmphesized !== false){
                textStyle.color = MUI.Styles.getMuiTheme().baseTheme.palette.primary1Color;;
            }
            return <TableHeaderColumn  style={headerStyle}><span style={textStyle}>{item[field]}</span></TableHeaderColumn>
        })
    },
    getFirstColumn(){
        let collection = this.props.scores;
        let field = 'name';

        if (this.props.settings.transpose){
            collection = this.props.categories;
            field = 'label';
        }

        return collection.map((item)=>{
            let textStyle = {};
            if (this.props.settings.transpose && item.isSummary){
                textStyle.color = MUI.Styles.getMuiTheme().baseTheme.palette.accent1Color;;
            } else if (this.props.settings.transpose && item.isEmphesized !== false){
                textStyle.color = MUI.Styles.getMuiTheme().baseTheme.palette.primary1Color;;
            }

            return (
                <TableRow>
                    <TableRowColumn><span style={textStyle}>{item[field]}</span></TableRowColumn>
                </TableRow>
            )
        })
    },
    getDataRows(){
        let cellStyle={
            textAlign: 'center'
        }

        let collection = this.props.scores;
        let innerCollection = this.props.categories
        if (this.props.settings.transpose){
            collection = this.props.categories;
            innerCollection = this.props.scores
        }

        return collection.map((item)=>{
            let cells = innerCollection.map((innerItem, index)=>{
                let score = this.props.settings.transpose ? innerItem : item;
                let category = this.props.settings.transpose ? item : innerItem;

                if (!score.hasOwnProperty(category.type)){
                    score[category.type] = category.defaultValue;
                }

                let ViewComponent = category.viewComponent;
                let isEmphesized = category.isEmphesized === false ? false : true;

                return (
                    <TableRowColumn style={cellStyle}>
                        <ViewComponent
                            style={{minWidth:50}}
                            onScoreEditRequested={this.handleScoreEditRequested.bind(this, score, category)}
                            onScoreChanged={this.handleScoreChanged.bind(this, score, category)}
                            onScoreChangeCompleted={this.props.onScoreChangeCompleted}
                            score={score[category.type]}
                            isEmphesized={isEmphesized}
                            isSummary={category.isSummary}
                        />
                    </TableRowColumn>
                );
            });

            return (
                <TableRow>
                    {cells}
                </TableRow>
            )
        });
    },
    getNumberOfColumns(){
        if (!this.props.settings.transpose) {
            return this.props.categories.length;
        } else {
            return this.props.scores.length;
        }
    },
    render(){
        const headerHeight = this.props.headerHeight;
        const firstColumnWidth = this.props.firstColumnWidth;
        const dataTableWidth = this.props.columnWidth * this.getNumberOfColumns();

        return (
            <div id="score-table" style={{flexGrow:1, display: 'flex', flexDirection: 'column', overflow:'hidden'}}>
                <div id="header" style={{display: 'flex', height:headerHeight, minHeight: headerHeight}}>
                    {/* top left cell */}
                    <Table
                        wrapperStyle={{height:headerHeight, width:firstColumnWidth, minWidth:firstColumnWidth, overflow:'visible'}}
                        bodyStyle={{overflow:'visible'}}
                        style={{width:firstColumnWidth, height:headerHeight}}
                        fixedHeader={false}
                        fixedFooter={false}
                    >
                        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                            <TableRow>
                                <TableHeaderColumn>
                                    <IconButton onTouchTap={this.handleSettingsClicked} tooltip="scorer settings">
                                        <FontIcon className="material-icons">settings</FontIcon>
                                    </IconButton>
                                    {this.props.isScored ? <IconButton onTouchTap={this.handleClearClicked} tooltip="clear scores">
                                        <FontIcon className="material-icons">clear</FontIcon>
                                    </IconButton> : null}
                                </TableHeaderColumn>
                            </TableRow>
                        </TableHeader>
                    </Table>
                    {/*top row */}
                    <Table
                        wrapperStyle={{height:headerHeight, overflow:'hidden'}}
                        bodyStyle={{position:'relative', top:0, left:0}}
                        style={{width:dataTableWidth, height:headerHeight}}
                        fixedHeader={false}
                        fixedFooter={false}
                        ref="rowHeader"
                    >
                        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                            <TableRow>
                                {this.getHeaders()}
                            </TableRow>
                        </TableHeader>
                    </Table>
                </div>
                <div id="data" style={{display: 'flex', flexGrow: 1, overflow:'hidden'}}>
                    {/*first column*/}
                    <Table
                        wrapperStyle={{width:firstColumnWidth, minWidth:firstColumnWidth, overflow:'hidden'}}
                        style={{width:firstColumnWidth}}
                        bodyStyle={{overflowX:'hidden', overflowY: 'hidden', position:'relative', top:0, left:0}}
                        fixedHeader={false}
                        fixedFooter={false}
                        ref="firstColumn"
                        selectable={false}
                    >
                        <TableBody displayRowCheckbox={false}>
                            {this.getFirstColumn()}
                        </TableBody>
                    </Table>
                    {/*data*/}
                    <Table
                        wrapperStyle={{}}
                        bodyStyle={{overflowX:'auto', overflowY: 'auto'}}
                        style={{width:dataTableWidth}}
                        fixedHeader={false}
                        fixedFooter={false}
                        ref="dataTable"
                        selectable={false}
                    >
                        <TableBody displayRowCheckbox={false}>
                            {this.getDataRows()}
                        </TableBody>
                    </Table>
                </div>
            </div>
        )
    }
});