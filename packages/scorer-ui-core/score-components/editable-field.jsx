const {RaisedButton} = MUI;

EditableField = React.createClass({
    handleScoreEditRequested(event){
        event.preventDefault();
        this.props.onScoreEditRequested();
    },
    render(){
        let value = this.props.score.toString();

        return (
            <RaisedButton
                style={this.props.style}
                onTouchTap={this.handleScoreEditRequested}
                label={value}
                primary={this.props.isSummary}
                secondary={this.props.isEmphesized}
            />
        )

    }
})