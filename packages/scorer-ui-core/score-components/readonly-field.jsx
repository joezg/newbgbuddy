ReadOnlyField = React.createClass({
    render(){
        let value = this.props.score.toString();

        let scoreStyle = {};

        if (this.props.isSummary){
            scoreStyle.color = MUI.Styles.getMuiTheme().baseTheme.palette.accent1Color;
        } else if (this.props.isEmphesized){
            scoreStyle.color = MUI.Styles.getMuiTheme().baseTheme.palette.primary1Color;
        }

        return (
            <div style={this.props.style}>
                <span style={scoreStyle}>{value}</span>
            </div>
        )

    }
})