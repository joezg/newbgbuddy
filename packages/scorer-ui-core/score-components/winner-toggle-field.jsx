const FontIcon = MUI.FontIcon;

WinnerToggleField = React.createClass({
    render(){
        return <ToggleField {...this.props} iconIfTrue="mood" iconIfFalse="remove"/>
    }
})