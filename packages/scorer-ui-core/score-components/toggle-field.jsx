const {FontIcon, IconButton} = MUI;

ToggleField = React.createClass({
    getDefaultProps(){
        return {
            score: false,
            onScoreChanged: ()=>{},
            iconIfTrue: 'check',
            iconIfFalse: 'close'
        }
    },
    handleToggle(){
        if (this.props.score === true || this.props.score === 'true'){
            this.props.onScoreChanged(false);
        } else {
            this.props.onScoreChanged(true);
        }

        this.props.onScoreChangeCompleted();
    },
    render(){
        let iconColor;

        if (this.props.isSummary){
            iconColor = MUI.Styles.getMuiTheme().baseTheme.palette.accent1Color;
        } else if (this.props.isEmphesized){
            iconColor = MUI.Styles.getMuiTheme().baseTheme.palette.primary1Color;
        }

        if (this.props.score === true || this.props.score === 'true'){
            return (
                <IconButton onTouchTap={this.handleToggle}>
                    <FontIcon className="material-icons" color={iconColor} style={this.props.style}>{this.props.iconIfTrue}</FontIcon>
                </IconButton>
            );
        } else {
            return (
                <IconButton onTouchTap={this.handleToggle}>
                    <FontIcon className="material-icons" color={iconColor} style={this.props.style}>{this.props.iconIfFalse}</FontIcon>
                </IconButton>
            );
        }
    }
})