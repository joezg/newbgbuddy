const {RaisedButton, FlatButton, IconButton} = MUI;

ScoreEdit = React.createClass({
    mixins: [MediaQueryMixin],
    getInitialState(){
        return {
            settingsVisible: false
        };
    },
    getDefaultProps(){
        return {
            score: {
                playerName: "unknown",
                score: 0
            },
            category: {
                type: 'score',
                label: 'Score'
            },
            onScoreChanged: ()=>{},
            onSettingsChanged: ()=>{},
            onScoreChangeCompleted: ()=>{},
            onSettingsChangeCompleted: ()=>{}
        }
    },
    handleScoreClick(event){
        event.preventDefault();
        this.props.onScoreChangeCompleted();
    },
    handleSettingsClick(event){
        event.preventDefault();
        if (this.state.settingsVisible){
            this.props.onSettingsChangeCompleted();
        }

        this.state.settingsVisible = !this.state.settingsVisible;
        this.setState(this.state);
    },
    getDefaultSettings(){
        return {
            smallIncrease: 1, largeIncrease:10
        };
    },
    handleScoreChanged(sign, type, event){
        event.preventDefault();
        let currentScore = this.props.score[this.props.category.type];
        let settings = this.props.settings.categories[this.props.category.type] || this.getDefaultSettings();

        let newScore;
        if (type === 'small'){
            newScore = currentScore + settings.smallIncrease * sign;
        } else {
            newScore = currentScore + settings.largeIncrease * sign;
        }

        this.props.onScoreChanged(this.props.score, this.props.category, newScore);
    },
    handleSettingsChanged(multiplyer, event){
        event.preventDefault();
        let settings = this.props.settings;
        let categorySettings = settings.categories[this.props.category.type] || this.getDefaultSettings();

        if (categorySettings.smallIncrease < 1)
            return;

        let newCategorySettings = {
            smallIncrease: Math.round(categorySettings.smallIncrease * multiplyer),
            largeIncrease: Math.round(categorySettings.largeIncrease * multiplyer)
        }

        settings.categories[this.props.category.type] = newCategorySettings;

        this.props.onSettingsChanged(settings);
    },
    render(){
        const buttonStyle = {minWidth:50},
            scoreStyle = {display:'inline-block', minWidth:50, width:50},
            decreaseSmallStyle = Object.assign({marginRight:5}, buttonStyle),
            decreaseBigStyle = Object.assign({marginRight:5}, buttonStyle),
            increaseSmallStyle = Object.assign({marginLeft:5}, buttonStyle),
            increaseBigStyle = Object.assign({marginLeft:5}, buttonStyle);

        let score = this.props.score[this.props.category.type].toString();

        const settingsButtons =
            <div>
                <FlatButton onTouchTap={this.handleSettingsChanged.bind(this, 10)}>Increase</FlatButton>
                <FlatButton onTouchTap={this.handleSettingsChanged.bind(this, 0.1)}>Decrease</FlatButton>
                <FlatButton onTouchTap={this.handleSettingsClick} primary={true}>Done</FlatButton>
            </div>

        let settings = this.props.settings.categories[this.props.category.type] || this.getDefaultSettings();

        let smallDecreaseLabel = "-"+settings.smallIncrease.toString();
        let largeDecreaseLabel = "-"+settings.largeIncrease.toString();
        let smallIncreaseLabel = "+"+settings.smallIncrease.toString();
        let largeIncreaseLabel = "+"+settings.largeIncrease.toString();

        return (
            <div>
                <IconButton onTouchTap={this.handleSettingsClick} style={{position:'absolute', right:0, top:-5}} iconClassName="material-icons" tooltip="setup">settings</IconButton>
                <RaisedButton onTouchTap={this.handleScoreChanged.bind(this, -1, 'large')} secondary={true} style={decreaseBigStyle} label={largeDecreaseLabel}/>
                <RaisedButton onTouchTap={this.handleScoreChanged.bind(this, -1, 'small')} secondary={true} style={decreaseSmallStyle} label={smallDecreaseLabel}/>
                <FlatButton onTouchTap={this.handleScoreClick} style={scoreStyle}>{score}</FlatButton>
                <RaisedButton onTouchTap={this.handleScoreChanged.bind(this, 1, 'small')} secondary={true} style={increaseSmallStyle} label={smallIncreaseLabel}/>
                <RaisedButton onTouchTap={this.handleScoreChanged.bind(this, 1, 'large')} secondary={true} style={increaseBigStyle} label={largeIncreaseLabel}/>
                {this.state.settingsVisible ? settingsButtons : null}
            </div>

        )
    }
})