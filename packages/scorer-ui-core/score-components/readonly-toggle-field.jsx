const FontIcon = MUI.FontIcon;

ReadOnlyToggleField = React.createClass({
    render(){
        let iconColor;

        if (this.props.isSummary){
            iconColor = MUI.Styles.getMuiTheme().baseTheme.palette.accent1Color;
        } else if (this.props.isEmphesized){
            iconColor = MUI.Styles.getMuiTheme().baseTheme.palette.primary1Color;
        }

        if (this.props.score === true || this.props.score === 'true'){
            return <FontIcon className="material-icons" color={iconColor} style={this.props.style}>{this.props.iconIfTrue}</FontIcon>;
        } else {
            return <FontIcon className="material-icons" color={iconColor} style={this.props.style}>{this.props.iconIfFalse}</FontIcon>
        }
    }
})