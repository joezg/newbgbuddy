const FontIcon = MUI.FontIcon;

WinnerField = React.createClass({
    render(){
        return <ReadOnlyToggleField {...this.props} iconIfTrue="mood" iconIfFalse=""/>
    }
})