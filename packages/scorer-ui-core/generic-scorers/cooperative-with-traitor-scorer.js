CooperativeWithTraitorScorer = function(){
    //this is used to render score table in score-table.jsx
    this.categories = [
        {type: 'isTraitor', label: 'Traitor', editComponent: null, viewComponent: ToggleField, defaultValue: false },
        {type: 'winner', label: 'Winner', editComponent: null, viewComponent: WinnerToggleField, defaultValue: false },
        {type: 'score', label: 'Score', editComponent: ScoreEdit, viewComponent: EditableField, defaultValue:0 }
    ];

    //this component will be used when settings is clicked in score-table
    this.settingsComponent = null;

    //these are internal settings to this scorer. Component above could chenge them
    this.settings = {
        categories: {}
    }
}

//this is called when category score changes
CooperativeWithTraitorScorer.prototype.scoreChanged = function(scores, score, category, newScore){
    let ix = scores.indexOf(score)

    if (category.type === 'isTraitor'){
        this.updateTraitor(scores[ix], scores, newScore);
    } else if (category.type === 'winner'){
        this.updateSimilarWinnerStatus(score, scores, newScore);
    } else if (category.type === 'score'){
        this.updateSimilarScore(score, scores, newScore);
    }

    return scores;
}

//this is called when scoring change is completed. used to calculate what needs to be calculated
CooperativeWithTraitorScorer.prototype.scoreChangeCompleted = function(scores){
    //nothing to do because all calculations are done in scoreChanged and only there they could be done
}

//this is called when overall scorer settings changes
CooperativeWithTraitorScorer.prototype.settingsChanged = function(newSettings, scores){
    Object.assign(this.settings, newSettings);
}

//this is called when overall scorer settings change is completed
CooperativeWithTraitorScorer.prototype.settingsChangeCompleted = function(scores){
    //nothing to do
}

//private
CooperativeWithTraitorScorer.prototype.updateTraitor = function(score, scores, isTraitor){
    let previousSimilarScore = scores.find((score)=> score.isTraitor === isTraitor);

    if (previousSimilarScore){
        score.score = previousSimilarScore.score;
        score.isTraitor = previousSimilarScore.isTraitor;
        score.winner = previousSimilarScore.winner;
    } else {
        score.score = 0;
        score.isTraitor = isTraitor;
        score.winner = !score.winner;
    }

}

//private
CooperativeWithTraitorScorer.prototype.updateSimilarWinnerStatus = function(score, scores, newWinnerStatus){
    let isTraitor = score.isTraitor;

    scores.forEach((score)=>{
        if (score.isTraitor === isTraitor){
            score.winner = newWinnerStatus;
        } else {
            score.winner = !newWinnerStatus;
        }
    });
}

//private
CooperativeWithTraitorScorer.prototype.updateSimilarScore = function(score, scores, newScore){
    let isTraitor = score.isTraitor;

    scores.forEach((score)=>{
        if (score.isTraitor === isTraitor){
            score.score = newScore;
        }
    });
}

AddScorer({
    _id: 'cooperativeTraitorScorer',
    name: 'Traitor game scorer',
    scorer: CooperativeWithTraitorScorer
})