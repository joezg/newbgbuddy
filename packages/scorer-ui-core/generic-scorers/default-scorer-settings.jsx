const {Toggle} = MUI;

DefaultScorerSettings = React.createClass({
    mixins: [MediaQueryMixin],
    getDefaultProps(){
        return {
            settings: {
                highestScoreWins: true,
                highestTiebreakerWins: true
            }
        }
    },
    handleToggle(type, event, newValue){
        event.preventDefault();

        let settings = this.props.settings;
        if (type === 'score'){
            settings.highestScoreWins = newValue;
        } else if (type === 'tiebreaker') {
            settings.highestTiebreakerWins = newValue;
        }

        this.props.onSettingsChanged(settings);
    },
    render(){
        return (
            <div>
                <Toggle
                    label="Highest score wins"
                    onToggle={this.handleToggle.bind(this, 'score')}
                    toggled={this.props.settings.highestScoreWins}
                />
                <Toggle
                    label="When tied, highest tiebreaker wins"
                    onToggle={this.handleToggle.bind(this, 'tiebreaker')}
                    toggled={this.props.settings.highestTiebreakerWins}
                />
            </div>

        )
    }
})