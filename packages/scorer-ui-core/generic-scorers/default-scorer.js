DefaultScorer = function(){
    //this is used to render score table in score-table.jsx
    this.categories = [
        {type: 'score', label: 'Score', editComponent: ScoreEdit, viewComponent: EditableField, defaultValue:0 },
        {type: 'tiebreaker', label: 'Tiebreaker', editComponent: ScoreEdit, viewComponent: EditableField, defaultValue:0, isEmphesized: false },
        {type: 'rank', label: 'Rank', editComponent: null, viewComponent: ReadOnlyField, defaultValue: 1, isSummary: true },
        {type: 'winner', label: 'Winner', editComponent: null, viewComponent: WinnerField, defaultValue: false, isSummary: true }
    ];

    //this component will be used when settings is clicked in score-table
    this.settingsComponent = DefaultScorerSettings;

    //these are internal settings to this scorer. Component above could chenge them
    this.settings = {
        highestScoreWins: true,
        highestTiebreakerWins: true,
        categories: {}
    }
}

//this is called when category score changes
DefaultScorer.prototype.scoreChanged = function(scores, score, category, newScore){
    let ix = scores.indexOf(score)

    scores[ix][category.type] = newScore;

    return this.setRankAndWinner(scores);
}

//this is called when scoring change is completed. used to calculate what needs to be calculated
DefaultScorer.prototype.scoreChangeCompleted = function(scores){
    this.setRankAndWinner(scores);
}

//this is called when overall scorer settings changes
DefaultScorer.prototype.settingsChanged = function(newSettings){
    Object.assign(this.settings, newSettings);
}

//this is called when overall scorer settings change is completed
DefaultScorer.prototype.settingsChangeCompleted = function(scores){
    this.setRankAndWinner(scores);
}

//this is private for this scorer which adjusts automatic fields (rank and winner)
DefaultScorer.prototype.setRankAndWinner = function(scores) {
    let sortedScores = [];
    for(let i=0; i<scores.length; i++){
        sortedScores.push({
            index: i,
            score: scores[i].score,
            tiebreaker: scores[i].tiebreaker
        })
    }

    sortedScores.sort((first, second)=> {
        let scoreDiff = second.score - first.score;
        if (!this.settings.highestScoreWins){
            scoreDiff *= -1;
        }

        if (scoreDiff !== 0){
            return scoreDiff;
        } else {
            scoreDiff = second.tiebreaker - first.tiebreaker
            if (!this.settings.highestTiebreakerWins){
                scoreDiff *= -1;
            }

            return scoreDiff;
        }


    })

    let currentScore = null;
    let currentTiebreaker = null
    let currentRank = 0;
    let rankSkip = 1;
    for(let i=0; i<sortedScores.length; i++){
        let idx = sortedScores[i].index;
        if (scores[idx].score === currentScore && scores[idx].tiebreaker === currentTiebreaker){
            scores[idx].rank = currentRank;
            rankSkip++;
        } else {
            currentRank = currentRank + rankSkip;
            scores[idx].rank = currentRank;
            currentScore = scores[idx].score;
            currentTiebreaker = scores[idx].tiebreaker;
            rankSkip = 1;
        }
        if (currentRank===1){
            scores[idx].winner = true;
        } else {
            scores[idx].winner = false;
        }
    }

    return scores;
}

AddScorer({
    _id: 'defaultScorer',
    name: 'Competitive game scorer',
    scorer: DefaultScorer
})