CooperativeScorer = function(){
    //this is used to render score table in score-table.jsx
    this.categories = [
        {type: 'winner', label: 'Winner', editComponent: null, viewComponent: WinnerToggleField, defaultValue: false },
        {type: 'score', label: 'Score', editComponent: ScoreEdit, viewComponent: EditableField, defaultValue:0 }
    ];

    //this component will be used when settings is clicked in score-table
    this.settingsComponent = null;

    //these are internal settings to this scorer. Component above could chenge them
    this.settings = {
        categories: {}
    }
}

CooperativeScorer.prototype = CooperativeWithTraitorScorer.prototype;

AddScorer({
    _id: 'cooperativeScorer',
    name: 'Cooperative game scorer',
    scorer: CooperativeScorer
})