Package.describe({
  name: 'jhladek:scorer-ui-core',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use([
    'ecmascript',
    'react',
    'izzilab:material-ui',
    'jhladek:react-media-query',
    'mongo'
  ], 'client');
  api.addFiles([
    'score-components/score-edit.jsx',
    'score-components/editable-field.jsx',
    'score-components/readonly-field.jsx',
    'score-components/readonly-toggle-field.jsx',
    'score-components/toggle-field.jsx',
    'score-components/winner-field.jsx',
    'score-components/winner-toggle-field.jsx',
    'scorers.js',
    'generic-scorers/default-scorer-settings.jsx',
    'generic-scorers/default-scorer.js',
    'generic-scorers/cooperative-with-traitor-scorer.js',
    'generic-scorers/cooperative-scorer.js',
    'score-table-components/inner-data-table.jsx',
    'score-table-components/score-table.jsx'
  ], 'client');
  api.export([
    'ScoreTable',
    'ScoreEdit',
    'EditableField',
    'ReadOnlyField',
    'ReadOnlyToggleField',
    'ToggleField',
    'WinnerField',
    'WinnerToggleField',
    'AddScorer'
  ], 'client');
});

