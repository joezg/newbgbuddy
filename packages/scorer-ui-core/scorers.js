Scorers = [];

AddScorer = function(scorer){
    if (!scorer._id)
        throw new Meteor.Error("illegal-state", `Scorer ${scorer.name} must have unique _id field defined!`);

    Scorers[scorer._id] = scorer;
}