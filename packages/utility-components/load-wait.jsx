let { CircularProgress, Overlay } = MUI;

Loading = React.createClass({
    render(){
        return (
            <div>
                <Overlay show={true} />
                <CircularProgress size={1.5} style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    margin: 'auto',
                    maxWidth: '100%',
                    maxHeight: '100%'
                }} />
            </div>

        )
    }
});

LoadingWait = React.createClass({
    render(){
        return (
            this.props.waitFor ? this.props.children : <Loading />
        )
    }
});