Package.describe({
    name: 'jhladek:utility-components',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: 'Loading widget made with material-ui',
    // URL to the Git repository containing the source code for this package.
    git: '',
    // By default, Meteor will default to using README.md for documentation.
    // To avoid submitting documentation, set this field to null.
    documentation: 'README.md'
});

Package.onUse(function (api) {
    api.versionsFrom('1.2.1');
    api.use([
        'ecmascript',
        'react',
        'izzilab:material-ui'
    ], 'client');
    api.addFiles(['load-wait.jsx', 'is-logged.jsx'], 'client');
    api.export([
        'LoadingWait', 'Loading', 'IsLogged'
    ], 'client')
});
