IsLogged = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    // Loads items from the Parts collection and puts them on this.data.parts
    getMeteorData() {
        return {
            isLogged: !!Meteor.userId()
        }
    },
    render(){
        return (
            this.data.isLogged
                ? this.props.children
                : (this.props.ifUnlogged ? this.props.ifUnlogged :  <Accounts.ui.LoginFormSet />)
        )
    }
});