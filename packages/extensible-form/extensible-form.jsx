let { FlatButton, Paper } = MUI;

ExtensibleForm = React.createClass({
    getInitialState(){
        return {
            extensionId: ''
        };
    },
    getDefaultProps(){
        return {
            extensions: [],
            extensionToShowId: ''
        }
    },
    componentWillMount(){
        this.state.extensionId = this.props.extensionToShowId;
    },
    handleExtensionChanged(extensionId, event){
        event.preventDefault();

        this.state.extensionId = extensionId
        this.setState(this.state);
    },
    handleExtensionActionInvoked(action, event){
        event.preventDefault();

        action(this.props);
    },
    renderExtension(){
        if (this.state.extensionId !== ''){
            let extension = this.props.extensions[this.state.extensionId];
            let ExtensionComponent = extension.component;

            if (ExtensionComponent){
                return <ExtensionComponent {...this.props}></ExtensionComponent>
            } else if (extension.rendering){
                return extension.rendering;
            } else {
                return null;
            }
        } else {
            return null;
        }

    },
    renderActions(){
        let actions = [];

        for (let extensionId in this.props.extensions){
            if (this.props.extensions.hasOwnProperty(extensionId) && extensionId !== this.state.extensionId){
                let ext = this.props.extensions[extensionId];

                let isEnabled = typeof ext.actionEnabled === 'function' ? ext.actionEnabled(this.props) : !!ext.actionEnabled;

                if (ext.action){
                    actions.push(<FlatButton disabled={!isEnabled} label={ext.actionName} onTouchTap={this.handleExtensionActionInvoked.bind(this, ext.action)} />);
                }

                if (ext.component || ext.rendering){
                    actions.push(<FlatButton disabled={!isEnabled} label={ext.actionName} onTouchTap={this.handleExtensionChanged.bind(this, extensionId)} />);
                }


            }
        }

        return actions;
    },

    render(){
        return (
            <Paper style={{flexGrow: 1, display:'flex', flexDirection:'column'}}>
                <div id="matchFormTitle" style={{padding:16}}>
                    <div style={{fontSize:15, color:'rgba(0,0,0,0.87)'}}>{this.props.title}</div>
                    <div style={{fontSize:14, color:'rgba(0,0,0,0.54)'}}>{this.props.subtitle}</div>
                </div>
                <div style={{padding:16, display:'flex', flexDirection:'column', flexGrow:1, overflow:'auto'}}>
                    {this.state.extensionId ? this.renderExtension() : null}
                </div>
                <div style={{padding:8}}>
                    {this.renderActions()}
                </div>
            </Paper>
        );
    }
})