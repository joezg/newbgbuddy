Package.describe({
  name: 'jhladek:first-player-picker',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'bgBuddy module for picking fist player',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use([
    'ecmascript',
    'react',
    'jhladek:parts-core'
  ], 'client');
  api.addFiles([
    'components/first-player-picker-main.jsx',
    'first-player-picker.js'
  ], 'client');
});
