AddPart({
    _id: "first-player-picker",
    routeGroup: "firstPlayerPicker",
    category: "Helper",
    title: "First player picker",
    subtitle: "Chooses a player for you",
    description: "Enter all names and let the app decide who will start first",
    routes: {
        "/": Main
    }
});

