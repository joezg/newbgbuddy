Meteor.methods({
    registerPartForUser(_id){
        check(_id, String);
        check(Meteor.userId(), String);

        UserParts.upsert(
            {
                userId: Meteor.userId()
            },
            {
                $push: {
                    userWidgets: _id
                }
            }
        )
    },
    unregisterPartForUser(_id){
        check(_id, String);
        check(Meteor.userId(), String);

        UserParts.update(
            {
                userId: Meteor.userId()
            },
            {
                $pull: {
                    userWidgets: _id
                }
            }
        )
    }
});