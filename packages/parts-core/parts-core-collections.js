if (Meteor.isClient){
    Parts = {};

    AddPart = function(part){
        if (!part._id)
            throw new Meteor.Error("illegal-state", `Part ${part.name} must have unique _id field defined!`);

        Parts[part._id] = part;
    }
}

UserParts = new Mongo.Collection('userParts');