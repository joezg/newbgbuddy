Package.describe({
  name: 'jhladek:parts-core',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Support for bgBuddy modules',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');

  api.use([
    'ecmascript',
    'mongo'
  ], ['client', 'server']);
  api.use([
    'react',
    'jhladek:utility-components',
    'izzilab:material-ui',
    'jhladek:react-media-query'
  ], 'client');

  api.addFiles([
    'parts-core-collections.js',
    'parts-core-methods.js'
  ], ['client', 'server']);
  api.addFiles([
    'parts-core-routes.jsx',
    'parts-core-components.jsx'
  ], 'client');
  api.addFiles([
    'parts-core-publications.js'
  ], 'server');

  api.export([
    'UserParts'
  ], ['client', 'server']);
  api.export([
    'UserWidgets',
    'AddPart',
    'SetupPartRoutes'
  ], 'client')

});

