SetupPartRoutes = function (router, mainLayout) {
    _.values(Parts).forEach((part)=> {
        for (let route in part.routes) {
            if (part.routes.hasOwnProperty(route)) {
                router.route('/' + part.routeGroup + route, {
                    action(params) {
                        let Component = part.routes[route];
                        ReactLayout.render(mainLayout, {content() { return <Component {...params} /> }});
                    }
                });
            }
        }
    })
};