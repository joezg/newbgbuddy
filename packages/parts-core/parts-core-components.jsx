let {
    Card,
    CardHeader,
    CardMedia,
    CardText,
    CardActions,
    CardTitle,
    FlatButton,
    MenuItem,
    IconButton,
    FontIcon,
    GridList,
    GridTile,
    Paper
    } = MUI;

UserWidgets = React.createClass({
    // This mixin makes the getMeteorData method work
    mixins: [ReactMeteorData],

    // Loads items from the Parts collection and puts them on this.data.parts
    getMeteorData() {
        let data = {
            userParts: [],
            allParts: [],
            loadingDone: false,
            user: Meteor.user()
        };

        var handle = Meteor.subscribe('userParts');

        if (handle.ready()) {
            data.loadingDone = true;
            data.userParts = [];
            if (Meteor.userId()) {
                let userParts = UserParts.findOne({userId: Meteor.userId()});

                if (userParts) {
                    userParts.userWidgets.forEach((widgetId)=>{
                        if (Parts.hasOwnProperty(widgetId)){
                            data.userParts.push(Parts[widgetId]);
                        }
                    })
                }
            }
        }

        data.allParts = Parts;

        return data;
    },
    getInitialState(){
        return {
            showAllWidgets: false
        }
    },
    showAllWidgets(){
        this.state.showAllWidgets = true;
        this.setState(this.state);
    },
    showUserWidgets(){
        this.state.showAllWidgets = false;
        this.setState(this.state);
    },
    setCustomMenu(){
        let menu = [];

        if (this.data.user){
            if (this.state.showAllWidgets)
                menu.push({text:"View your favourite widgets", callback:this.showUserWidgets })
            else
                menu.push({text:"View all Widgets", callback:this.showAllWidgets })
        }

        AppMenu.setCustomItems(menu);
    },
    render() {
        this.setCustomMenu();

        let showUserPartsOnly = this.data.user && !this.state.showAllWidgets;
        let waitFor = this.data.user ? this.data.loadingDone : true;
        return (
            <LoadingWait waitFor={waitFor}>
                <Widgets allParts={this.data.allParts} userParts={this.data.userParts} showUserPartsOnly={showUserPartsOnly}/>
            </LoadingWait>
        );

    }
});

const favouriteButtonStyle = {
    position: 'absolute',
    right: 30,
    bottom: 5
}

const Widgets = React.createClass({
    mixins: [MediaQueryMixin],
    addUserWidget(part){
        Meteor.call('registerPartForUser', part._id);
    },
    removeUserWidget(part){
        Meteor.call('unregisterPartForUser', part._id);
    },
    getPartsToShow(){
        if (this.props.showUserPartsOnly)
            return this.props.userParts;

        return this.props.allParts;
    },
    renderWidgets(){
        return _.map(this.getPartsToShow(), (function (part, i) {
            const actions = [
                <FlatButton onTouchTap={()=>FlowRouter.go('/'+part.routeGroup+'/')}>Start</FlatButton>
            ];

            let favouriteIcon = "favorite_border";
            let tooltip = "Not favourite";
            let callback = this.addUserWidget.bind(this, part);

            if (this.props.showUserPartsOnly || this.props.userParts.find((p) => p._id === part._id)){
                favouriteIcon = "favorite";
                tooltip = "Favourite";
                callback = this.removeUserWidget.bind(this, part);
            }

            actions.push(<IconButton tooltip={tooltip} tooltipPosition="top-center" onTouchTap={callback} style={favouriteButtonStyle}><FontIcon className="material-icons">{favouriteIcon}</FontIcon></IconButton>);

            return (
                <GridTile
                    key={part._id}
                    rootClass={Paper}
                >
                    <Card style={{height:'100%', minHeight:'100%'}}>
                        <CardHeader
                            title={part.title}
                            subtitle={part.category}
                        />
                        <CardText>
                            {part.description}
                        </CardText>
                        <CardActions style={{position:'absolute', bottom:10, width:'100%' }}>
                            {actions}
                        </CardActions>
                    </Card>
                </GridTile>
            )
        }).bind(this));
    },
    getDefaultProps() {
        return {
            allWidgets:false
        };
    },
    render(){
        let cols;
        if (this.state.media.mobile){
            cols = 1;
        } else if (this.state.media.tablet){
            cols = 2;
        } else if (this.state.media.desktop){
            cols = 3;
        } else {
            cols = 4;
        }
        return (
            <GridList
                cellHeight={300}
                cols={cols}
                padding={8}
                style={{minHeight:300}}
            >
                {this.renderWidgets()}
            </GridList>
        );
    }
})