const {Paper, IconButton, FontIcon} = MUI;
const {blue900, deepPurple500, yellow600, grey400, green800, red700, black, brown500, grey200, white} = MUI.Styles.Colors;

BonusTilesScore = new React.createClass({
    _updateStateFromProps(props){
        this.state.score = props.score;
        if (!this.state.score.bonusTilesItems){
            this.state.score.bonusTilesItems = []
        }
    },
    componentWillMount(){
        this._updateStateFromProps(this.props);
    },
    componentWillReceiveProps(nextProps){
        this._updateStateFromProps(nextProps);
    },
    getInitialState(){
        return {};
    },
    handleTileClick(tileId, event){
        event.preventDefault();
        bonusTilesItems = this.state.score.bonusTilesItems
        bonusTilesItems.push(tileId);

        this.props.onScoreChanged(this.props.score, this.props.category, bonusTilesItems);
    },
    handleRemove(tile, event){
        event.preventDefault();
        bonusTilesItems = this.state.score.bonusTilesItems;

        let ix = bonusTilesItems.indexOf(tile)
        bonusTilesItems.splice(ix, 1);

        this.props.onScoreChanged(this.props.score, this.props.category, bonusTilesItems);
    },
    handleTileMouseEnter(tile){
        this.state.hoveredTile = tile;
        this.setState(this.state)
    },
    handleTileMouseLeave(){
        this.state.hoveredTile = null;
        this.setState(this.state)
    },
    getTileColors(color){
        switch (color){
            case 'blue':
                return {tileColor: blue900, fontColor: white};
            case 'brown':
                return {tileColor: brown500, fontColor: white};
            case 'white':
                return {tileColor: grey200, fontColor: black};
            case 'yellow':
                return {tileColor: yellow600, fontColor: black};
            case 'black':
                return {tileColor: black, fontColor: white};
            case 'purple':
                return {tileColor: deepPurple500, fontColor: white};
            case 'green':
                return {tileColor: green800, fontColor: white};
            case 'red':
                return {tileColor: red700, fontColor: white};
        }
    },
    renderTile(tile, cb){
        let defaultStyle = {
            height: 70,
            width: 70,
            margin: 10,
            textAlign: 'center',
            fontSize: 32,
            color: black
        }

        let style = {};
        let vpValueStyle = {};

        if (tile.type === 'level'){
            style.backgroundColor = 'white';
            style.height = 100;
            style.width = 100;
            vpValueStyle.marginTop = 10;
        } else if (tile.type === 'family'){
            let {tileColor, fontColor} = this.getTileColors(tile.color);
            style.backgroundColor = tileColor;
            style.color = fontColor;
            vpValueStyle.marginTop = 17;
        }

        style = _.defaults(style, defaultStyle);

        return (
            <Paper zDepth={this.state.hoveredTile===tile ? 4 : 2} style={style} circle="true" onMouseEnter={this.handleTileMouseEnter.bind(this, tile)} onMouseLeave={this.handleTileMouseLeave} onTouchTap={cb}>
                {tile.level ? <div style={{color: grey400, fontSize:24, marginTop: 8}}>{tile.level}</div> : null}
                <div style={vpValueStyle}>{tile.value}</div>
            </Paper>
        )
    },
    renderPlayerBonusTiles(){
        if (this.state.score.bonusTilesItems.length > 0){
            return this.state.score.bonusTilesItems.map((tileId)=>{
                return this.renderTile(this.props.settings.bonusTiles[tileId], this.handleRemove.bind(this, tileId))
            })
        }  else {
            return null;
        }
    },
    renderAvailableBonusTiles(){
        let tiles = [];

        for (tileId in this.props.settings.bonusTiles){
            if (this.props.settings.bonusTiles.hasOwnProperty(tileId)){
                let tile = this.props.settings.bonusTiles[tileId];
                if (tile.available){
                    tiles.push(this.renderTile(tile, this.handleTileClick.bind(this, tileId)))
                }
            }
        }
        return tiles;
    },
    render(){
        return (
            <main>
                Available tiles
                <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                    {this.renderAvailableBonusTiles()}
                </div>
                Score: <strong>{this.state.score.bonusTiles}</strong>
                <div>Player's tiles</div>
                <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap'}}>
                    {this.renderPlayerBonusTiles()}
                </div>
            </main>
        );
    }

});