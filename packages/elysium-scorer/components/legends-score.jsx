const {Paper, IconButton, FontIcon} = MUI;
const {blue900, deepPurple500, yellow600, green800, red700} = MUI.Styles.Colors;

LegendsScore = new React.createClass({
    _updateStateFromProps(props){
        this.state.score = props.score;
        if (!this.state.score.legendItems){
            this.state.score.legendItems = []
        }
    },
    getInitialState(){
        return {
            highlightedLevelCard: 0,
            highlightedFamilyCard: 0
        }
    },
    componentWillMount(){
        this._updateStateFromProps(this.props);
    },
    componentWillReceiveProps(nextProps){
        this._updateStateFromProps(nextProps);
    },
    handleLegendClick(type, level){
        event.preventDefault();
        legendItems = this.state.score.legendItems
        legendItems.push({type, level});

        this.props.onScoreChanged(this.props.score, this.props.category, legendItems);
    },
    highlight(type, level){
        if (type === 'level'){
            this.state.highlightedLevelCard = level
        }  else if (type === 'family'){
            this.state.highlightedFamilyCard = level
        }
        this.setState(this.state);
    },
    unhiglight(){
        this.state.highlightedFamilyCard = 0;
        this.state.highlightedLevelCard = 0;
        this.setState(this.state);
    },
    handleRemove(item, event){
        event.preventDefault();
        legendItems = this.state.score.legendItems;

        let ix = legendItems.indexOf(item)
        legendItems.splice(ix, 1);

        this.props.onScoreChanged(this.props.score, this.props.category, legendItems);
    },
    renderLegendItems(){
        if (this.state.score.legendItems.length > 0){
            return this.state.score.legendItems.map((item)=>{
                return <div>
                    {item.level} cards {item.type === 'family' ? 'Family' : 'Level'} legend ({item.points} points)
                    <IconButton style={{top:5, padding:0, width: 'auto', height: 'auto'}} onTouchTap={this.handleRemove.bind(this, item)}>
                        <FontIcon className="material-icons">close</FontIcon>
                    </IconButton>
                </div>;
            })
        }  else {
            return null;
        }
    },
    render(){
        let pickerStyle = {
            display: 'flex',
            flexDirection: 'row'
        }

        let cardStyle = {
            width: 50,
            height: 85,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            margin: 5,
            borderRadius: 6,
            color: 'white'
        }

        let getCardStyle = function(bgcolor){
            let style = {backgroundColor: bgcolor}

            if (bgcolor === yellow600){
                style.color = 'black'
            }

            return _.defaults(style, cardStyle);
        }

        return (
            <div>
                Add Level legend
                <div style={pickerStyle}>
                    <Paper style={getCardStyle(deepPurple500)} zDepth={this.state.highlightedLevelCard < 2 ? 1 : 3} onMouseEnter={this.highlight.bind(this, 'level', 2)} onMouseLeave={this.unhiglight} onTouchTap={this.handleLegendClick.bind(this, 'level', 2)}>2</Paper>
                    <Paper style={getCardStyle(yellow600)} zDepth={this.state.highlightedLevelCard < 3 ? 1 : 3} onMouseEnter={this.highlight.bind(this, 'level', 3)} onMouseLeave={this.unhiglight} onTouchTap={this.handleLegendClick.bind(this, 'level', 3)}>3</Paper>
                    <Paper style={getCardStyle(green800)} zDepth={this.state.highlightedLevelCard < 4 ? 1 : 3} onMouseEnter={this.highlight.bind(this, 'level', 4)} onMouseLeave={this.unhiglight} onTouchTap={this.handleLegendClick.bind(this, 'level', 4)}>4</Paper>
                    <Paper style={getCardStyle(red700)} zDepth={this.state.highlightedLevelCard < 5 ? 1 : 3} onMouseEnter={this.highlight.bind(this, 'level', 5)} onMouseLeave={this.unhiglight} onTouchTap={this.handleLegendClick.bind(this, 'level', 5)}>5</Paper>
                </div>
                Add Family legend
                <div style={pickerStyle}>
                    <Paper style={getCardStyle(blue900)} zDepth={this.state.highlightedFamilyCard < 2 ? 1 : 3} onMouseEnter={this.highlight.bind(this, 'family', 2)} onMouseLeave={this.unhiglight} onTouchTap={this.handleLegendClick.bind(this, 'family', 2)}>2</Paper>
                    <Paper style={getCardStyle(blue900)} zDepth={this.state.highlightedFamilyCard < 3 ? 1 : 3} onMouseEnter={this.highlight.bind(this, 'family', 3)} onMouseLeave={this.unhiglight} onTouchTap={this.handleLegendClick.bind(this, 'family', 3)}>3</Paper>
                </div>
                Score: <strong>{this.state.score.legends}</strong>
                <div>
                    {this.renderLegendItems()}
                </div>
            </div>
        );
    }

});