ElysiumScorer = function () {
    //this is used to render score table in score-table.jsx
    this.categories = [
        {type: 'vpChips', label: "VP Chips", editComponent: ScoreEdit, viewComponent: EditableField, defaultValue: 0},
        {type: 'legends', label: "Legends", editComponent: LegendsScore, viewComponent: EditableField, defaultValue: 0},
        {
            type: 'bonusTiles',
            label: "Bonus Tiles",
            editComponent: BonusTilesScore,
            viewComponent: EditableField,
            defaultValue: 0
        },
        {
            type: 'chronosPowers',
            label: "Chronos Powers",
            editComponent: ScoreEdit,
            viewComponent: EditableField,
            defaultValue: 0
        },
        {
            type: 'prestigeChips',
            label: "Prestige (chips)",
            editComponent: ScoreEdit,
            viewComponent: EditableField,
            defaultValue: 0,
            isEmphesized: false
        },
        {
            type: 'prestigePoints',
            label: "Prestige Points",
            editComponent: null,
            viewComponent: ReadOnlyField,
            defaultValue: 0
        },
        {
            type: 'citizens',
            label: "Citizens",
            editComponent: ScoreEdit,
            viewComponent: EditableField,
            defaultValue: 0,
            isEmphesized: false
        },
        {
            type: 'citizensPoints',
            label: "Citizens points",
            editComponent: null,
            viewComponent: ReadOnlyField,
            defaultValue: 0
        },
        {
            type: 'tiebreaker',
            label: "Gold (tiebreaker)",
            editComponent: ScoreEdit,
            viewComponent: EditableField,
            defaultValue: 0,
            isEmphesized: false
        },
        {
            type: 'score',
            label: 'Total',
            editComponent: null,
            viewComponent: ReadOnlyField,
            defaultValue: 0,
            isSummary: true
        },
        {
            type: 'rank',
            label: 'Rank',
            editComponent: null,
            viewComponent: ReadOnlyField,
            defaultValue: 1,
            isSummary: true
        },
        {
            type: 'winner',
            label: 'Winner',
            editComponent: null,
            viewComponent: WinnerField,
            defaultValue: false,
            isSummary: true
        }
    ];

    //this component will be used when settings is clicked in score-table
    this.settingsComponent = null;

    //these are internal settings to this scorer. Component above could chenge them
    this.settings = {
        transpose: true,
        bonusTiles: {
            level3: {value: 9, available: true, type: 'level', level: 3},
            level2: {value: 6, available: true, type: 'level', level: 2},
            level1: {value: 3, available: true, type: 'level', level: 1},
            blueBig: {value: 5, available: true, type: 'family', color: 'blue', size: 'big'},
            blueSmall: {value: 2, available: true, type: 'family', color: 'blue', size: 'small'},
            brownBig: {value: 5, available: true, type: 'family', color: 'brown', size: 'big'},
            brownSmall: {value: 2, available: true, type: 'family', color: 'brown', size: 'small'},
            whiteBig: {value: 5, available: true, type: 'family', color: 'white', size: 'big'},
            whiteSmall: {value: 2, available: true, type: 'family', color: 'white', size: 'small'},
            yellowBig: {value: 5, available: true, type: 'family', color: 'yellow', size: 'big'},
            yellowSmall: {value: 2, available: true, type: 'family', color: 'yellow', size: 'small'},
            blackBig: {value: 5, available: true, type: 'family', color: 'black', size: 'big'},
            blackSmall: {value: 2, available: true, type: 'family', color: 'black', size: 'small'},
            purpleBig: {value: 5, available: true, type: 'family', color: 'purple', size: 'big'},
            purpleSmall: {value: 2, available: true, type: 'family', color: 'purple', size: 'small'},
            greenBig: {value: 5, available: true, type: 'family', color: 'green', size: 'big'},
            greenSmall: {value: 2, available: true, type: 'family', color: 'green', size: 'small'},
            redBig: {value: 5, available: true, type: 'family', color: 'red', size: 'big'},
            redSmall: {value: 2, available: true, type: 'family', color: 'red', size: 'small'}
        },
        categories: {
            vpChips: {smallIncrease: 1, largeIncrease: 5},
        }
    }
}

//this is called when category score changes
ElysiumScorer.prototype.scoreChanged = function (scores, score, category, newScore) {
    let ix = scores.indexOf(score)
    let calclulateAll = false;

    if (category.type === 'legends') {
        this.calculateLegendsScore(scores[ix], newScore);
    } else if (category.type === 'bonusTiles') {
        this.calculateBonusTilesScore(scores[ix], newScore);
        this.setAvailableTiles(scores);
    } else {
        scores[ix][category.type] = newScore;
    }

    if (category.type === 'citizens') {
        scores[ix].citizensPoints = scores[ix].citizens * -2;
    }
}

//this is called when overall scorer settings changes
ElysiumScorer.prototype.settingsChanged = function (newSettings, scores) {
    Object.assign(this.settings, newSettings);
    return scores;
}

//this is called when scoring change is completed. used to calculate what needs to be calculated
ElysiumScorer.prototype.scoreChangeCompleted = function (scores) {
    this.calculatePrestige(scores);

    scores.forEach(function (score) {
        score.score =
            score.vpChips +
            score.legends +
            score.bonusTiles +
            score.chronosPowers +
            score.prestigePoints +
            score.citizensPoints;
    })

    this.setRankAndWinner(scores);
}

//this is called when overall scorer settings change is completed
ElysiumScorer.prototype.settingsChangeCompleted = function (scores) {
}


//this is private for this scorer which adjusts automatic fields (rank and winner)
ElysiumScorer.prototype.setRankAndWinner = function (scores) {
    let sortedScores = [];
    for (let i = 0; i < scores.length; i++) {
        sortedScores.push({
            index: i,
            score: scores[i].score,
            tiebreaker: scores[i].tiebreaker
        })
    }

    sortedScores.sort((first, second)=> {
        let scoreDiff = second.score - first.score;
        if (scoreDiff !== 0) {
            return scoreDiff;
        } else {
            scoreDiff = second.tiebreaker - first.tiebreaker
            return scoreDiff;
        }


    })

    let currentScore = null;
    let currentTiebreaker = null
    let currentRank = 0;
    let rankSkip = 1;
    for (let i = 0; i < sortedScores.length; i++) {
        let idx = sortedScores[i].index;
        if (scores[idx].score === currentScore && scores[idx].tiebreaker === currentTiebreaker) {
            scores[idx].rank = currentRank;
            rankSkip++;
        } else {
            currentRank = currentRank + rankSkip;
            scores[idx].rank = currentRank;
            currentScore = scores[idx].score;
            currentTiebreaker = scores[idx].tiebreaker;
            rankSkip = 1;
        }
        if (currentRank === 1) {
            scores[idx].winner = true;
        } else {
            scores[idx].winner = false;
        }
    }

    return scores;
}

ElysiumScorer.prototype.calculatePrestige = function (scores) {
    let sortedScores = [];
    for (let i = 0; i < scores.length; i++) {
        sortedScores.push({
            index: i,
            prestigeChips: scores[i].prestigeChips
        })
    }

    sortedScores.sort((first, second)=> {
        return second.prestigeChips - first.prestigeChips;
    });

    //if at least one player have some prestige chips, prestige points are calculated
    if (sortedScores[0].prestigeChips !== 0) {
        let points = [16, 8, 4, 2];

        let currentPrestige = sortedScores[0].prestigeChips;
        let pointsToShare = 0;
        let numberOfSharingPlayers = 0;
        let firstIndexToGetPoints = 0;
        for (let i = 0; i < sortedScores.length; i++) {
            let idx = sortedScores[i].index;
            if (scores[idx].prestigeChips === currentPrestige) {
                pointsToShare += points.shift();
                numberOfSharingPlayers++;
            }

            //the last one with different score
            if (i === (sortedScores.length - 1) && scores[idx].prestigeChips != currentPrestige) {
                scores[idx].prestigePoints = points.shift();
            }

            //if last index, always calculate
            //else calculate if new prestige points
            if (scores[idx].prestigeChips !== currentPrestige || i === (sortedScores.length - 1)) {
                let pointsToAdd = Math.floor(pointsToShare / numberOfSharingPlayers);
                for (let j = 0; j < numberOfSharingPlayers; j++) {
                    let indexToGivePoints = sortedScores[firstIndexToGetPoints++].index;
                    scores[indexToGivePoints].prestigePoints = pointsToAdd;
                }
                currentPrestige = scores[idx].prestigeChips;
                pointsToShare = points.shift();
                numberOfSharingPlayers = 1;
            }
        }
    }

    for (let i = 0; i < scores.length; i++) {
        if (scores[i].prestigeChips === 0) {
            scores[i].prestigePoints = 0;
        }
    }
}

ElysiumScorer.prototype.calculateBonusTilesScore = function (score, tileItems) {
    score.bonusTiles = 0;
    score.bonusTilesItems = tileItems;

    for (let i = 0; i < tileItems.length; i++) {
        score.bonusTiles += this.settings.bonusTiles[tileItems[i]].value;
    }
};

ElysiumScorer.prototype.setAvailableTiles = function(scores){
    _.values(this.settings.bonusTiles).forEach((tile)=>{
        tile.available = true;
    });

    scores.forEach((score)=>{
        if (score.bonusTilesItems){
            score.bonusTilesItems.forEach((tileId)=>{
                this.settings.bonusTiles[tileId].available = false;
            })
        }
    })
};

ElysiumScorer.prototype.calculateLegendsScore = function (score, legendItems) {
    score.legends = 0;
    score.legendItems = legendItems;

    for (let i = 0; i < legendItems.length; i++) {
        if (legendItems[i].type === 'level') {
            switch (legendItems[i].level) {
                case 2:
                {
                    legendItems[i].points = 2;
                    break;
                }
                case 3:
                {
                    legendItems[i].points = 4;
                    break;
                }
                case 4:
                {
                    legendItems[i].points = 8;
                    break;
                }
                case 5:
                {
                    legendItems[i].points = 12;
                    break;
                }
            }
        } else if (legendItems[i].type === 'family') {
            switch (legendItems[i].level) {
                case 2:
                {
                    legendItems[i].points = 3;
                    break;
                }
                case 3:
                {
                    legendItems[i].points = 6;
                    break;
                }
            }
        }


        score.legends += legendItems[i].points;
    }
}

AddScorer({
    _id: 'elysiumScorer',
    gameName: 'Elysium',
    name: 'Elysium scorer',
    scorer: ElysiumScorer
})